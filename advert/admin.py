# -*- coding: utf-8 -*-
from django.contrib import admin
from advert.models import Advert
from django.contrib.auth.models import User


class AdvertAdmin(admin.ModelAdmin):
    class Meta:
        model = Advert

    def user_email(self, instance):
        return instance.advert_user.email

    def __unicode__(self):
        return self.email

    list_display = ['advert_name', 'advert_user', 'advert_price_from', 'user_email', 'advert_views', 'advert_date_pub',
                    'advert_date_update', 'advert_active']

admin.site.register(Advert, AdvertAdmin)
