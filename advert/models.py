# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# import os
from django.db import models
from django.contrib.auth.models import User
from imagekit.models.fields import ImageSpecField
from imagekit.processors import ResizeToFill
from django_resized import ResizedImageField


class Advert(models.Model):
    class Meta:
        ordering = ['-advert_date_pub', ]
        verbose_name = 'объявление'
        verbose_name_plural = 'объявления'

    PROPERTY_TYPE = (
        (20, 'база отдыха'),
        (40, 'дача'),
        (60, 'дом (частный сектор)'),
        (80, 'квартира'),
        (100, 'комната'),
        (120, 'коттедж'),
        (140, 'отель'),
        (160, 'пансионат'),
        (180, 'санаторий'),
    )

    DISTRICT = (
        (20, '8 марта'),
        (40, 'азмол'),
        (60, 'акз'),
        (80, 'верховая'),
        (100, 'военный городок'),
        (120, 'дачи'),
        (140, 'колония'),
        (160, 'коса ближняя'),
        (180, 'коса средняя '),
        (200, 'коса дальняя '),
        (220, 'курорт'),
        (240, 'лиски'),
        (260, 'макорты'),
        (280, 'нагорная часть'),
        (300, 'пески'),
        (320, 'ртс'),
        (340, 'слободка'),
        (360, 'стекловолокно'),
        (380, 'третий пляж'),
        (400, 'центр'),
    )

    ROOMS = (
        (20, 'одна комната',),
        (40, 'две комнаты',),
        (60, 'три комнаты',),
        (80, 'четыре и более',),
    )

    advert_name = models.CharField(max_length=65, verbose_name='заголовок', )
    advert_content = models.TextField(max_length=1500, blank=True, verbose_name='текст описания', )
    advert_district = models.PositiveSmallIntegerField(choices=DISTRICT, db_index=True, blank=True, null=True,
                                                       verbose_name='район города', )
    advert_property_type = models.PositiveSmallIntegerField(choices=PROPERTY_TYPE, db_index=True, default=4,
                                                            verbose_name='объект недвижимости', )
    advert_room = models.PositiveSmallIntegerField(choices=ROOMS, db_index=True, blank=True, null=True,
                                                   verbose_name='количество комнат', )
    advert_seat = models.DecimalField(db_index=True, max_digits=2, decimal_places=0, blank=True, null=True,
                                      verbose_name='количество спальных мест', )
    advert_price_from = models.DecimalField(max_digits=5, decimal_places=0, db_index=True, default=100,
                                            verbose_name='цена', )
    advert_price_to = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True,
                                          verbose_name='цена до (если несколько вариантов)', )
    advert_address = models.CharField(max_length=100, blank=True, verbose_name='адрес', )
    advert_telephone = models.CharField(default='4535345', max_length=18, verbose_name='телефон', )
    advert_conditioner = models.BooleanField(default=False, verbose_name='кондиционер', )
    advert_tv = models.BooleanField(default=False, verbose_name='телевизор', )
    advert_shower = models.BooleanField(default=False, verbose_name='душ', )
    advert_wi_fi = models.BooleanField(default=False, verbose_name='wi-fi', )
    advert_food = models.BooleanField(default=False, verbose_name='питание', )
    advert_animals = models.BooleanField(default=False, verbose_name='заселение с животными', )
    advert_parking = models.BooleanField(default=False, verbose_name='автостоянка', )
    advert_balcony = models.BooleanField(default=False, verbose_name='балкон', )
    advert_satellite_tv = models.BooleanField(default=False, verbose_name='спутниковое TV', )
    advert_playground = models.BooleanField(default=False, verbose_name='детская площадка', )
    advert_washer = models.BooleanField(default=False, verbose_name='стиральная машинка', )
    advert_brazier = models.BooleanField(default=False, verbose_name='мангал', )
    advert_author = models.CharField(max_length=50, verbose_name='ваше имя', blank=True, )
    advert_views = models.PositiveIntegerField(default=0, verbose_name='количество просмотров', )
    advert_like = models.PositiveIntegerField(default=0, verbose_name='понравилось', )
    advert_dislike = models.PositiveIntegerField(default=0, verbose_name='не понравилось', )
    advert_date_pub = models.DateTimeField(auto_now_add=True, auto_now=False, db_index=True,
                                           verbose_name='дата публикации', )
    advert_date_update = models.DateTimeField(auto_now_add=False, auto_now=True, db_index=True,
                                              verbose_name='дата редактирования', )
    advert_title = models.TextField(max_length=75, verbose_name='Title', blank=True, )
    advert_descriptions = models.TextField(max_length=200, verbose_name='Descriptions', blank=True, )
    advert_keywords = models.TextField(max_length=150, verbose_name='Keywords', blank=True, )
    advert_slug = models.SlugField(verbose_name='URL', blank=True, )
    advert_user = models.ForeignKey(User, verbose_name='автор', )
    advert_active = models.BooleanField(default=True, db_index=True, verbose_name='объявление активное', )


class Photo(models.Model):
    class Meta:
        ordering = ['-photo_date_pub']
        verbose_name = 'фото'
        verbose_name_plural = 'фотки'

    advert = models.ForeignKey(Advert, null=True)
    photo_origin = ResizedImageField(upload_to='uploaded_photo/%Y/%m/%d/', size=[1280, 720], quality=80,
                                     db_index=True, )
    photo_350 = ImageSpecField([ResizeToFill(350, 250)],
                               source='photo_origin',
                               format='JPEG',
                               options={'quality': 75}
                               )
    photo_240 = ImageSpecField([ResizeToFill(240, 130)],
                               source='photo_origin',
                               format='JPEG',
                               options={'quality': 75}
                               )
    photo_date_pub = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='дата публикации', )
    photo_date_update = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name='дата редактирования', )

























    # advert_photo = models.ImageField(upload_to='uploaded_photo_user/%Y/%m/%d/', height_field=500, width_field=500, )
    # advert_photo_300 = ImageSpecField([ResizeToFill(300, 300)], source='advert_photo', format='JPEG',
    #                                   options={'quality': 95})
    # advert_photo_140 = ImageSpecField([ResizeToFill(140, 140)], source='advert_photo2', format='JPEG',
    #                                   options={'quality': 95})
    # advert_photo_80 = ImageSpecField([ResizeToFill(80, 80)], source='advert_photo2', format='JPEG',
    #                                   options={'quality': 95})
    # advert_email = models.EmailField(max_length=100, blank=True, verbose_name='email', )



    # advert_user = models.ForeignKey(User, verbose_name='автор', editable=False)

    # def __unicode__(self):
    #     return self.advert_name


    # class AdvertPhoto(models.Model):
    #     image_height = models.PositiveIntegerField(null=True, blank=True, editable=False, default="500")
    #     image_width = models.PositiveIntegerField(null=True, blank=True, editable=False, default="500")
    #     advert_photo = models.ImageField(upload_to='uploaded_photo_user/%Y/%m/%d/',
    #                                      height_field='image_height', width_field='image_width', )
    #     advert_photo_300 = ImageSpecField([ResizeToFill(300, 300)], source='advert_photo', format='JPEG',
    #                                       options={'quality': 95})
