# -*- coding: utf-8 -*-
from django.shortcuts import render

from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response, redirect
from advert.models import Advert, Photo
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, FormView
from django.views.generic.edit import UpdateView
from forms import AdvertForm, PhotoForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.db.models import F
from django.http import JsonResponse
from django.views import View
import time
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response


class AdvertAllView(ListView):
    model = Advert
    paginate_by = 9
    template_name = 'advert_all.html'
    context_object_name = 'context_advert_all'
    queryset = Advert.objects.all()


class AdvertFilterPriceUPView(ListView):
    model = Advert
    paginate_by = 9
    template_name = 'filters/advert_filter_price_up.html'
    context_object_name = 'context_advert_filter_price_up'
    queryset = Advert.objects.all().order_by('advert_price_from')


class AdvertFilterPriceDownView(ListView):
    model = Advert
    paginate_by = 9
    template_name = 'filters/advert_filter_price_down.html'
    context_object_name = 'context_advert_filter_price_down'
    queryset = Advert.objects.all().order_by('-advert_price_from')


class AdvertFilterPropertyTypeUPView(ListView):
    model = Advert
    paginate_by = 9
    template_name = 'filters/advert_filter_property_type_up.html'
    context_object_name = 'context_advert_filter_property_type_up'
    queryset = Advert.objects.all().order_by('advert_property_type')


class AdvertFilterPropertyTypeDownView(ListView):
    model = Advert
    paginate_by = 9
    template_name = 'filters/advert_filter_property_type_down.html'
    context_object_name = 'context_advert_filter_property_type_down'
    queryset = Advert.objects.all().order_by('-advert_property_type')


class AdvertOneView(DetailView):
    model = Advert
    template_name = 'advert_one.html'

    def get(self, request, *args, **kwargs):
        self.counter = Advert.objects.get(pk=self.kwargs['pk'])
        self.counter.advert_views = F('advert_views') + 1
        self.counter.save()
        return super(AdvertOneView, self).get(request, *args, **kwargs)


class AdvertCabinetView(LoginRequiredMixin, ListView):
    login_url = '/login/'
    model = Advert
    template_name = 'cabinet.html'
    context_object_name = 'context_cabinet'
    paginate_by = 6

    def get_queryset(self):
        context = Advert.objects.filter(advert_user=self.request.user)
        return context


def successful(request):
    return render_to_response('successful.html')


class AdvertUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    model = Advert
    form_class = AdvertForm
    template_name = 'advert_update.html'

    def get_success_url(self):
        return reverse('advert:one', args=(self.kwargs['pk'],))

    def get_object(self, *args, **kwargs):
        object = super(AdvertUpdateView, self).get_object(*args, **kwargs)
        if object.advert_user != self.request.user:
            raise Http404
        return object


class AdvertAddView(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    template_name = 'advert_add.html'
    form_class = AdvertForm
    success_url = ('/advert/successful/')

    def get_context_data(self, **kwargs):
        context = super(AdvertAddView, self).get_context_data(**kwargs)
        context['photo_origin'] = Photo.objects.all()
        return context

    # def post(self, request):
    #     time.sleep(1)
    #     form = PhotoForm(self.request.POST, self.request.FILES)
    #     if form.is_valid():
    #         photo = form.save()
    #         data = {'is_valid': True, 'name': photo.photo_origin.name, 'url': photo.photo_origin.url}
    #     else:
    #         data = {'is_valid': False}
    #     return JsonResponse(data)

    def form_valid(self, form):
        advert = form.save(commit=False)
        advert.advert_user = self.request.user
        advert = Advert(self.request.POST)
        advert = form.save()
        return super(AdvertAddView, self).form_valid(form)

    # def get(self, request):
    #     photos_list = Photo.objects.all()
    #     return render(self.request, 'advert_add.html', {'photos': photos_list})

    # def get(self, request):
    #     photos_list = Photo.objects.all()
    #     return render(self.request, 'advert_add.html', {'photos': photos_list})
    #
    # def post(self, request):
    #     time.sleep(1)
    #     form = PhotoForm(self.request.POST, self.request.FILES)
    #     if form.is_valid():
    #         photo = form.save()
    #         data = {'is_valid': True, 'name': photo.photo_origin.name, 'url': photo.photo_origin.url}
    #     else:
    #         data = {'is_valid': False}
    #     return JsonResponse(data)





# class AdvertPhotoAddView(LoginRequiredMixin, View):
#     login_url = '/login/'
#
#     def get(self, request):
#         photos_list = Photo.objects.all()
#         return render(self.request, 'advert_add_photo.html', {'photos': photos_list})
#
#     def post(self, request):
#         time.sleep(
#             1)  # You don't need this line. This is just to delay the process so you can see the progress bar testing locally.
#         form = PhotoForm(self.request.POST, self.request.FILES)
#         if form.is_valid():
#             photo = form.save()
#             data = {'is_valid': True, 'name': photo.photo_origin.name, 'url': photo.photo_origin.url}
#         else:
#             data = {'is_valid': False}
#         return JsonResponse(data)












# class AdvertPhotoAddView(LoginRequiredMixin, View):
#     login_url = '/login/'
#     def get(self, request):
#         photos_list = Photo.objects.all()
#         return render(self.request, 'advert_add.html', {'photos': photos_list})
#
#     def post(self, request):
#         time.sleep(1)  # You don't need this line. This is just to delay the process so you can see the progress bar testing locally.
#         form = PhotoForm(self.request.POST, self.request.FILES)
#         if form.is_valid():
#             photo = form.save()
#             data = {'is_valid': True, 'name': photo.photo_origin.name, 'url': photo.photo_origin.url}
#         else:
#             data = {'is_valid': False}
#         return JsonResponse(data)














def clear_database(request):
    for photo in Photo.objects.all():
        photo.photo_origin.delete()
        photo.delete()
    return redirect(request.POST.get('next'))














    # class AdvertAddView(LoginRequiredMixin, CreateView):
    #     context_object_name = 'advert_form'
    #     login_url = '/login/'
    #     template_name = 'advert_add.html'
    #     form_class = AdvertForm
    #     success_url = ('/advert/successful/')
    #
    #     def form_valid(self, form):
    #         advert = form.save(commit=False)
    #         advert.advert_user = self.request.user
    #         advert = Advert(self.request.POST)
    #         advert = form.save()
    #         return super(AdvertAddView, self).form_valid(form)


    # class PhotoAddView(LoginRequiredMixin, CreateView):
    #     login_url = '/login/'
    #     template_name = 'advert_add.html'
    #     form_class = PhotoForm
    #     success_url = ('/advert/successful/')
    #
    #     def form_valid(self, form):
    #         photo = form.save(commit=False)
    #         photo = Photo(self.request.POST, self.request.FILES)
    #         photo = form.save()
    #         return super(PhotoAddView, self).form_valid(form)


    # def model_form_upload(request):
    #     if request.method == 'POST':
    #         form = PhotoForm(request.POST, request.FILES)
    #         if form.is_valid():
    #             form.save()
    #             return redirect('/advert/all/')
    #     else:
    #         form = PhotoForm()
    #     return render(request, 'advert_add.html', {
    #         'form1': form
    #     })













    # class AdvertAddView(LoginRequiredMixin, CreateView):
    #     login_url = '/login/'
    #     template_name = 'advert_add.html'
    #     form_class = AdvertForm
    #     success_url = ('/advert/successful/')
    #
    #     def form_valid(self, form):
    #         advert = form.save(commit=False)
    #         advert.advert_user = self.request.user
    #         advert = Advert(self.request.POST, self.request.FILES)
    #         advert = form.save()
    #         return super(AdvertAddView, self).form_valid(form)
    #
    #





    # class AdvertAddView(LoginRequiredMixin, CreateView):
    #     login_url = '/login/'
    #     template_name = 'advert_add.html'
    #     form_class = AdvertForm
    #     success_url = ('/advert/successful/')
    #
    #     def form_valid(self, form):
    #         advert = form.save(commit=False)
    #         advert.advert_user = self.request.user
    #         advert = Advert(self.request.POST, self.request.FILES)
    #         advert = form.save()
    #         return super(AdvertAddView, self).form_valid(form)



























































































    # def cabinet(request):
    #     context = Advert.objects.all()[:8]
    #     return render_to_response('cabinet.html', {'context':context})




    # '/blog/details/%s/' % pk

    # def get(self, request, *args, **kwargs):
    #     self.context = Advert.objects.get(pk=self.kwargs['pk'])
    #     self.form = AdvertForm(instance=self.context)
    #     return super(AdvertUpdateView, self).get(request, *args, **kwargs)
    #
    # def form_valid(self, form):
    #     advert = form.save(commit=False)
    #     advert.advert_user = self.request.user
    #     advert = Advert(self.request.POST, self.request.FILES)
    #     advert = form.save()
    #     return super(AdvertUpdateView, self).form_valid(form)
    #
    #
    #







    #
    # class AdvertAddView(LoginRequiredMixin, CreateView):
    #     template_name = 'advert_add.html'
    #     form_class = AdvertForm
    #     success_url = ('/advert/successful/')
    #     login_url = '/login/'
    #
    #     def form_valid(self, form):
    #         advert = form.save(commit=False)
    #         advert.advert_user = self.request.user
    #         advert = form.save()
    #         return super(AdvertAddView, self).form_valid(form)















# class AdvertAddView(LoginRequiredMixin, CreateView):
#     login_url = '/login/'
#     template_name = 'advert_add.html'
#     form_class = AdvertForm
#     success_url = ('/advert/successful/')
#
#     def form_valid(self, form):
#         advert = form.save(commit=False)
#         advert.advert_user = self.request.user
#         advert = Advert(self.request.POST, self.request.FILES)
#         advert = form.save()
#         return super(AdvertAddView, self).form_valid(form)
#
#     def get(self, request):
#         photos_list = Photo.objects.all()
#         return render(self.request, 'advert_add.html', {'photos': photos_list})
#
#     def post(self, request):
#         time.sleep(1)
#         form = PhotoForm(self.request.POST, self.request.FILES)
#         if form.is_valid():
#             photo = form.save()
#             data = {'is_valid': True, 'name': photo.photo_origin.name, 'url': photo.photo_origin.url}
#         else:
#             data = {'is_valid': False}
#         return JsonResponse(data)











