# -*- coding: utf-8 -*-

from django.conf.urls import url
from views import AdvertAllView, AdvertOneView, AdvertFilterPriceUPView, AdvertFilterPriceDownView, \
    AdvertFilterPropertyTypeUPView, AdvertFilterPropertyTypeDownView
from views import AdvertUpdateView, successful, AdvertCabinetView, clear_database, AdvertAddView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^all/$', AdvertAllView.as_view(), name='all'),
    url(r'^(?P<pk>\d+)/$', AdvertOneView.as_view(), name='one'),
    url(r'^clear/$', clear_database, name='clear_database'),
    url(r'^filter_price_up/$', AdvertFilterPriceUPView.as_view(), name='filter_price_up'),
    url(r'^filter_price_down/$', AdvertFilterPriceDownView.as_view(), name='filter_price_down'),
    url(r'^filter_property_type_up/$', AdvertFilterPropertyTypeUPView.as_view(), name='filter_property_type_up'),
    url(r'^filter_property_type_down/$', AdvertFilterPropertyTypeDownView.as_view(), name='filter_property_type_down'),
    url(r'^add/$', AdvertAddView.as_view(), name='add'),
    url(r'^update/(?P<pk>\d+)/$', AdvertUpdateView.as_view(), name='update'),
    url(r'^successful/',  successful, name='successful')
    # url(r'^add/$', AdvertAddView.as_view(), name='add'),

    # url(r'^add/$', model_form_upload, name='add'),
]

