# -*- coding: utf-8 -*-
from django.forms import ModelForm
from models import Advert, Photo



class AdvertForm(ModelForm):
    class Meta:
        model = Advert
        fields = ['advert_name',
                  'advert_content',
                  'advert_district',
                  'advert_property_type',
                  'advert_room',
                  'advert_seat',
                  'advert_price_from',
                  'advert_price_to',
                  'advert_address',
                  'advert_telephone',
                  'advert_conditioner',
                  'advert_tv',
                  'advert_shower',
                  'advert_wi_fi',
                  'advert_food',
                  'advert_animals',
                  'advert_parking',
                  'advert_balcony',
                  'advert_satellite_tv',
                  'advert_playground',
                  'advert_washer',
                  'advert_brazier',
                  'advert_author',
                  ]


# class PhotoForm(ModelForm):
#     class Meta:
#         model = Photo
#         fields = ['photo_origin']

class PhotoForm(ModelForm):
    class Meta:
        model = Photo
        fields = ['photo_origin']