# -*- coding: utf-8 -*-
"""rest_brd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin


from django.conf.urls import include, url
from django.contrib import admin
from auth.views import signup
from django.contrib.auth.views import login, logout
from advert.views import successful
from django.conf import settings
from django.conf.urls.static import static
from advert.views import AdvertCabinetView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^advert/', include('advert.urls', namespace='advert')),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^signup/$', signup, name='signup'),
    url(r'^login/$', login, {'template_name': 'auth/login.html'}, name='login', ),
    url(r'^logout/$', logout, {'template_name': 'auth/logout.html', 'next_page': '/advert/all/'}, name='logout'),
    url(r'^cabinet/',  AdvertCabinetView.as_view(), name='cabinet'),
    # url(r'^photos/', include('photos.urls', namespace='photos')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)