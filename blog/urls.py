# -*- coding: utf-8 -*-
from django.conf.urls import url
from blog.views import blog_one, add_comment
from blog.views import BlogAllView

urlpatterns = [
    url(r'^all/$', BlogAllView.as_view(), name='all'),
    url(r'^(?P<pk>[0-9]+)/$', blog_one, name='one'),
    url(r'^add_comment/(?P<pk>[0-9]+)$', add_comment, name='add_comment',)
]

