# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


class Blog(models.Model):
    class Meta:
        ordering = ['-blog_date_pub', 'blog_name']
        verbose_name = 'блог'
        verbose_name_plural = 'блоги'

    blog_name = models.CharField(max_length=100, verbose_name='название', )
    blog_content = models.TextField(verbose_name='текст статьи', blank=True, null=True, )
    blog_author = models.CharField(max_length=50, default='Viacheslav Kardash', verbose_name='автор', blank=True,
                                   null=True, )
    blog_date_pub = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='дата пубикации', blank=True, null=True, )
    blog_date_update = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name='дата редактирования',
                                            blank=True, null=True, )
    blog_title = models.CharField(max_length=75, verbose_name='Title', blank=True, null=True, )
    blog_descriptions = models.TextField(max_length=200, verbose_name='Descriptions', blank=True, null=True, )
    blog_keywords = models.TextField(max_length=150, verbose_name='Keywords', blank=True, null=True, )
    blog_slug = models.SlugField(verbose_name='URL', blank=True, null=True, )

    def __unicode__(self):
        return self.blog_name


class Comments(models.Model):
    class Meta:
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии'
        ordering = []

    comments_article = models.ForeignKey(Blog)
    comments_text = models.TextField(verbose_name='текст комментрия', max_length=10000)
    comments_pub_date = models.DateTimeField(default=timezone.now, verbose_name='дата публикации')

    def __unicode__(self):
        return self.comments_text
