# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response, redirect
from blog.models import Blog, Comments
from blog.forms import CommentForm
from django.views.generic.list import ListView
from django.template.context_processors import csrf


class BlogAllView(ListView):
    model = Blog
    paginate_by = 5
    template_name = 'blog_all.html'
    context_object_name = 'context_blog_all'
    queryset = Blog.objects.all()


def blog_one(request, pk=1):
    comment_form = CommentForm
    args = {}
    args.update(csrf(request))
    args['key_blog_one'] = Blog.objects.get(pk=pk)
    args['key_comments'] = Comments.objects.filter(comments_article_id=pk)
    args['form'] = comment_form
    return render_to_response('blog_one.html', args)


def add_comment(request, pk):
    if request.POST:
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.comments_article = Blog.objects.get(pk=pk)
            form.save()
    return redirect('/blog/%s/' % pk)

