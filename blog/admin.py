# -*- coding: utf-8 -*-


from django.contrib import admin
from blog.models import Blog, Comments

admin.site.register(Blog)
admin.site.register(Comments)
